class Api::V1::ActivitiesController < ApplicationController

    def index
        @activities = Container.find(params[:id]).activity
        render json: @activities
    end

    def create
        @activity = Activity.new(activity_params)

        if @activity.save 
            render json: @activity,  status: :created
        else 
            render status: :unprocessable_entity
        end
    end

    def show
        @activity = ActivityRepairList.find_by(activity_id: params[:id])
        render json: @activity
    end

    def update
        @activity = Activity.find(params[:id])
        old_status_ = @activity.status

        params["activity"].each do |key, value|
            @activity[key] = value
        end
        if @activity.save and @activity.status != old_status_
            log = @activity.logs.create!(old_status: old_status_, new_status: @activity.status)
            render json: log, status: :ok, each_serializer: LogSerializer
        else
            render json: {error: "unable to update activity details"}, status: :unprocessable_entity
        end
    end

    private 

    def create_activity_params
        params.require()
    end
    
end
